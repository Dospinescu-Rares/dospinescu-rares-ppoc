package com.example.benchmarks;

import com.example.operations.DoubleOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DoubleBenchmark {
    @State(Scope.Benchmark)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 2, time = 1)
    @Fork(1)
    public static class BenchmarkState {
        private List<Double> doubleList = new ArrayList<>();;
        public int size = 100000000;

        @Param({"RANDOM", "ASCENDING", "DESCENDING"})
        public OrderType order;

        public enum OrderType {
            RANDOM, ASCENDING, DESCENDING
        }

        @Setup(Level.Iteration)
        public void setup() {
            switch (order) {
                case RANDOM:
                    doubleList = generateRandomList(size);
                    break;
                case ASCENDING:
                    doubleList = generateAscendingList(size);
                    break;
                case DESCENDING:
                    doubleList = generateDescendingList(size);
                    break;
            }
        }

        private List<Double> generateRandomList(int size) {
            Random random = new Random();
            return IntStream.range(0, size)
                    .mapToObj(i -> 1000 * random.nextDouble())
                    .collect(Collectors.toList());
        }

        private List<Double> generateAscendingList(int size) {
            return IntStream.range(0, size)
                    .mapToObj(Double::valueOf)
                    .collect(Collectors.toList());
        }

        private List<Double> generateDescendingList(int size) {
            return IntStream.range(0, size)
                    .mapToObj(i -> (double) (size - i))
                    .collect(Collectors.toList());
        }

        @Benchmark
        public void sumDouble() {
            DoubleOperations.sum(doubleList);
        }

        @Benchmark
        public void averageDouble() {
            DoubleOperations.average(doubleList);
        }

        @Benchmark
        public void topTenPercentDouble() {
            DoubleOperations.topTenPercentBiggest(doubleList);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(DoubleBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .result("Laboratory03/src/main/java/com/example/benchmarks/DoubleBenchmarkResults.csv")
                .build();

        new Runner(opt).run();
    }
}
