package com.example.benchmarks;

import com.example.operations.PrimitiveDoubleOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

public class PrimitiveDoubleBenchmark {
    @State(Scope.Benchmark)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 2, time = 1)
    @Fork(1)
    public static class BenchmarkState {
        private double[] primitiveDoubleArray;
        public int size = 100000000;

        @Param({"RANDOM", "ASCENDING", "DESCENDING"})
        public OrderType order;

        public enum OrderType {
            RANDOM, ASCENDING, DESCENDING
        }

        @Setup(Level.Iteration)
        public void setup() {
            switch (order) {
                case RANDOM:
                    primitiveDoubleArray = generateRandomList(size);
                    break;
                case ASCENDING:
                    primitiveDoubleArray = generateAscendingList(size);
                    break;
                case DESCENDING:
                    primitiveDoubleArray = generateDescendingList(size);
                    break;
            }
        }

        private double[] generateRandomList(int size) {
            Random random = new Random();
            return random.doubles(size)
                    .map(d -> d * 1000)
                    .toArray();
        }

        private double[] generateAscendingList(int size) {
            return DoubleStream.iterate(0, i -> i + 1)
                    .limit(size)
                    .toArray();
        }

        private double[] generateDescendingList(int size) {
            return DoubleStream.iterate(size, i -> i - 1)
                    .limit(size)
                    .toArray();
        }

        @Benchmark
        public void sumPrimitiveDouble() {
            PrimitiveDoubleOperations.sum(primitiveDoubleArray);
        }

        @Benchmark
        public void averagePrimitiveDouble() {
            PrimitiveDoubleOperations.average(primitiveDoubleArray);
        }

        @Benchmark
        public void topTenPercentPrimitiveDouble() {
            PrimitiveDoubleOperations.topTenPercentBiggest(primitiveDoubleArray);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PrimitiveDoubleBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .result("Laboratory03/src/main/java/com/example/benchmarks/PrimitiveDoubleBenchmarkResults.csv")
                .build();

        new Runner(opt).run();
    }
}
