package com.example.benchmarks;

import com.example.operations.BigDecimalOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BigDecimalBenchmark {
    @State(Scope.Benchmark)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 2, time = 1)
    @Fork(1)
    public static class BenchmarkState {
        private List<BigDecimal> bigDecimalList = new ArrayList<>();
        public int size = 100000000;

        @Param({"RANDOM", "ASCENDING", "DESCENDING"})
        public OrderType order;

        public enum OrderType {
            RANDOM, ASCENDING, DESCENDING
        }

        @Setup(Level.Iteration)
        public void setup() {
            switch (order) {
                case RANDOM:
                    bigDecimalList = generateRandomList(size);
                    break;
                case ASCENDING:
                    bigDecimalList = generateAscendingList(size);
                    break;
                case DESCENDING:
                    bigDecimalList = generateDescendingList(size);
                    break;
            }
        }

        private List<BigDecimal> generateRandomList(int size) {
            Random random = new Random();
            return IntStream.range(0, size)
                    .mapToObj(i -> BigDecimal.valueOf(1000 * random.nextDouble()))
                    .collect(Collectors.toList());
        }

        private List<BigDecimal> generateAscendingList(int size) {
            return IntStream.range(0, size)
                    .mapToObj(BigDecimal::valueOf)
                    .collect(Collectors.toList());
        }

        private List<BigDecimal> generateDescendingList(int size) {
            return IntStream.range(0, size)
                    .mapToObj(i -> BigDecimal.valueOf(size - i))
                    .collect(Collectors.toList());
        }

        @Benchmark
        public void sumBigDecimal() {
            BigDecimalOperations.sum(bigDecimalList);
        }

        @Benchmark
        public void averageBigDecimal() {
            BigDecimalOperations.average(bigDecimalList);
        }

        @Benchmark
        public void topTenPercentBigDecimal() {
            BigDecimalOperations.topTenPercentBiggest(bigDecimalList);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(BigDecimalBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .result("Laboratory03/src/main/java/com/example/benchmarks/BigDecimalBenchmarkResults.csv")
                .build();

        new Runner(opt).run();
    }
}
