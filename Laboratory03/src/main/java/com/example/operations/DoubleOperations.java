package com.example.operations;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class DoubleOperations {
    public static double sum(List<Double> elements) {
        return elements.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public static double average(List<Double> elements) {
        if (elements.isEmpty()) {
            return 0.0;
        }

        return elements.parallelStream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public static List<Double> topTenPercentBiggest(List<Double> elements) {
        int count = (int) Math.ceil(elements.size() * 0.1);
        return elements.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList());
    }
}
