package com.example.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class BigDecimalOperations {
    public static BigDecimal sum(List<BigDecimal> elements) {
        return elements.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> elements) {
        if (elements.isEmpty()) {
            return BigDecimal.ZERO;
        }

        return elements.parallelStream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(elements.size()), 2, RoundingMode.HALF_UP);
    }

    public static List<BigDecimal> topTenPercentBiggest(List<BigDecimal> elements) {
        int count = (int) Math.ceil(elements.size() * 0.1);
        return elements.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList());
    }
}

