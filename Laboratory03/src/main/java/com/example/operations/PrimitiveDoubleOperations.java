package com.example.operations;

import java.util.Arrays;
import java.util.Comparator;


public class PrimitiveDoubleOperations {
    public static double sum(double[] elements) {
        return Arrays.stream(elements).sum();
    }

    public static double average(double[] elements) {
        if (elements.length == 0) {
            return 0.0;
        }

        return sum(elements) / elements.length;
    }

    public static double[] topTenPercentBiggest(double[] elements) {
        int count = (int) Math.ceil(elements.length * 0.1);
        return Arrays.stream(elements)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .mapToDouble(Double::doubleValue)
                .toArray();
    }
}

