package com.example;

import com.example.operations.BigDecimalOperations;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigDecimalOperationsTest {

    @Test
    public void testSum() {
        List<BigDecimal> elements = Arrays.asList(
                new BigDecimal("22.79"),
                new BigDecimal("61.01"),
                new BigDecimal("18.22")
        );
        assertEquals(new BigDecimal("102.02"), BigDecimalOperations.sum(elements));
    }

    @Test
    public void testSumEmptyList() {
        List<BigDecimal> elements = List.of();
        assertEquals(BigDecimal.ZERO, BigDecimalOperations.sum(elements));
    }

    @Test
    public void testAverage() {
        List<BigDecimal> elements = Arrays.asList(
                new BigDecimal("15.00"),
                new BigDecimal("10.00"),
                new BigDecimal("20.00")
        );
        assertEquals(new BigDecimal("15.00"), BigDecimalOperations.average(elements));
    }

    @Test
    public void testAverageEmptyList() {
        List<BigDecimal> elements = List.of();
        BigDecimal expectedAverage = BigDecimal.ZERO;
        assertEquals(expectedAverage, BigDecimalOperations.average(elements));
    }

    @Test
    public void testTopTenPercentBiggest() {
        List<BigDecimal> elements = Arrays.asList(
                new BigDecimal("19.00"),
                new BigDecimal("22.00"),
                new BigDecimal("5.00"),
                new BigDecimal("28.00"),
                new BigDecimal("25.00"),
                new BigDecimal("12.00")
        );
        List<BigDecimal> expectedTopTenPercent = List.of(
                new BigDecimal("28.00")
        );
        assertEquals(expectedTopTenPercent, BigDecimalOperations.topTenPercentBiggest(elements));
    }
}
