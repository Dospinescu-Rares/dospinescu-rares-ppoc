package com.example;

import com.example.operations.DoubleOperations;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DoubleOperationsTest {

    @Test
    public void testSum() {
        List<Double> elements = Arrays.asList(22.79, 61.01, 18.22);
        assertEquals(102.02, DoubleOperations.sum(elements));
    }

    @Test
    public void testSumEmptyList() {
        List<Double> elements = Arrays.asList();
        assertEquals(0.0, DoubleOperations.sum(elements));
    }

    @Test
    public void testAverage() {
        List<Double> elements = Arrays.asList(15.00, 10.00, 20.00);
        assertEquals(15.0, DoubleOperations.average(elements));
    }

    @Test
    public void testAverageEmptyList() {
        List<Double> elements = Arrays.asList();
        assertEquals(0.0, DoubleOperations.average(elements));
    }

    @Test
    public void testTopTenPercentBiggest() {
        List<Double> elements = Arrays.asList(19.00, 22.00, 5.0, 28.00, 25.0, 12.0);
        assertEquals(Arrays.asList(28.0), DoubleOperations.topTenPercentBiggest(elements));
    }
}
