package com.example;
import com.example.operations.PrimitiveDoubleOperations;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrimitiveDoubleOperationsTest {

    @Test
    public void testSum() {
        double[] elements = {22.79, 61.01, 18.22};
        assertEquals(102.02, PrimitiveDoubleOperations.sum(elements));
    }

    @Test
    public void testSumEmptyArray() {
        double[] elements = {};
        assertEquals(0.0, PrimitiveDoubleOperations.sum(elements));
    }

    @Test
    public void testAverage() {
        double[] elements = {15.00, 10.00, 20.00};
        assertEquals(15.0, PrimitiveDoubleOperations.average(elements));
    }

    @Test
    public void testAverageEmptyArray() {
        double[] elements = {};
        assertEquals(0.0, PrimitiveDoubleOperations.average(elements));
    }

    @Test
    public void testTopTenPercentBiggest() {
        double[] elements = {19.00, 22.00, 5.0, 28.00, 25.0, 12.0};
        assertArrayEquals(new double[]{28.0}, PrimitiveDoubleOperations.topTenPercentBiggest(elements));
    }
}

