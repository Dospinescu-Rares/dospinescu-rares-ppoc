package com.example;

import java.util.HashSet;
import java.util.Set;


public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> list = new HashSet<T>();

    public HashSetBasedRepository() { }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }
}

