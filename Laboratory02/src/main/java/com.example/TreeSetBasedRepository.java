package com.example;

import java.util.Set;
import java.util.TreeSet;


public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> list = new TreeSet<T>();

    public TreeSetBasedRepository() { }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

}

