package com.example;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;


public class MutableSetEclipseBasedRepository<T> implements InMemoryRepository<T> {
    private MutableSet<T> set = Sets.mutable.empty();

    public MutableSetEclipseBasedRepository() { }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }
}

