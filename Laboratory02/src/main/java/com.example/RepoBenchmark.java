package com.example;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;


public class RepoBenchmark {

    @State(Scope.Benchmark)
    public static class Initialization {
        private static final int LIST_SIZE = 10000;
        public static InMemoryRepository<Order> ArrayListRepository = new ArrayListBasedRepository<>();
        public static InMemoryRepository<Order> HashSetRepository = new HashSetBasedRepository<>();
        public static InMemoryRepository<Order> TreeSetRepository = new TreeSetBasedRepository<>();
        public static InMemoryRepository<Order> ConcHashMapRepository = new ConcurrentHashMapBasedRepository<>();
        public static InMemoryRepository<Order> EclipseListRepository = new EclipseCollectionsBasedRepository<>();
        public static InMemoryRepository<Order> EclipseSetRepository = new MutableSetEclipseBasedRepository<>();

        public static Order currentOrder = Order.getRandomOrder();

        @Setup(Level.Invocation)
        public void doSetup() {
            int i = 0;
            while (++i < LIST_SIZE) {
                ArrayListRepository.add(Order.getRandomOrder());
                HashSetRepository.add(Order.getRandomOrder());
                TreeSetRepository.add(Order.getRandomOrder());
                ConcHashMapRepository.add(Order.getRandomOrder());
                EclipseListRepository.add(Order.getRandomOrder());
                EclipseSetRepository.add(Order.getRandomOrder());
            }
        }

        @TearDown(Level.Iteration)
        public void doTearDown() { }
    }


    // Benchmarks for the 'add' method
    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testArrayListAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ArrayListRepository;
        list.add(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testHashSetAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.HashSetRepository;
        list.add(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testTreeSetAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.TreeSetRepository;
        list.add(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testConcHashMapAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ConcHashMapRepository;
        list.add(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseListAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseListRepository;
        list.add(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseSetAdd(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseSetRepository;
        list.add(Initialization.currentOrder);
    }

    // Benchmarks for the 'remove' method
    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testArrayListRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ArrayListRepository;
        list.remove(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testHashSetRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.HashSetRepository;
        list.remove(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testTreeSetRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.TreeSetRepository;
        list.remove(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testConcHashMapRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ConcHashMapRepository;
        list.remove(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseListRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseListRepository;
        list.remove(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseSetRemove(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseSetRepository;
        list.remove(Initialization.currentOrder);
    }


    // Benchmarks for the 'contains' method
    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testArrayListContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ArrayListRepository;
        list.contains(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testTreeSetContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.TreeSetRepository;
        list.contains(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testHashSetContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.HashSetRepository;
        list.contains(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testConcHashMapContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.ConcHashMapRepository;
        list.contains(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseListContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseListRepository;
        list.contains(Initialization.currentOrder);
    }

    @Benchmark
    @Measurement(batchSize = 1000, time = 5)
    public void testEclipseSetContains(final Initialization init) {
        InMemoryRepository<Order> list = Initialization.EclipseSetRepository;
        list.contains(Initialization.currentOrder);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(RepoBenchmark.class.getSimpleName())
                .warmupIterations(2)
                .measurementIterations(2)
                .forks(1)
                .resultFormat(ResultFormatType.CSV)
                .result("Laboratory02/src/main/java/com.example/benchmark_results.csv")
                .build();

        new Runner(opt).run();
    }
}
