package com.example;

import java.util.Objects;
import java.util.Random;


public class Order implements Comparable<Order> {
    private int id;
    private int price;
    private int quantity;


    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Order order = (Order) o;
        return id == order.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Order with id " + id + ", price " + price + " and quantity " + quantity;
    }

    @Override
    public int compareTo(Order o) {
        return id - o.id;
    }

    public static Order getRandomOrder() {
        Random rand = new Random();
        return new Order(rand.nextInt(100), rand.nextInt(100), rand.nextInt(100));
    }
}
