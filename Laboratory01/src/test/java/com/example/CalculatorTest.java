package com.example;

import com.example.Service.CalculatorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class CalculatorTest {
    private CalculatorService cs;

    @BeforeEach
    void setUp() {
        this.cs = new CalculatorService();
    }

    @Test
    public void testAddition() {
        Assertions.assertEquals(5, cs.execute("+", 2, 3));
    }

    @Test
    public void testSubtraction() {
        Assertions.assertEquals(1, cs.execute("-", 4, 3));
    }

    @Test
    public void testMultiplication() {
        Assertions.assertEquals(12, cs.execute("*", 3, 4));
    }

    @Test
    public void testDivision() {
        Assertions.assertEquals(4, cs.execute("/", 16, 4));
    }

    @Test
    public void testDivisionByZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> cs.execute("/", 16, 0));
    }

    @Test
    public void testMinimum() {
        Assertions.assertEquals(2, cs.execute("min", 2, 3));
    }

    @Test
    public void testMaximum() {
        Assertions.assertEquals(5, cs.execute("max", 2, 5));
    }

    @Test
    public void testSquareRoot() {
        Assertions.assertEquals(5, cs.execute("sqrt", 25, 0));
    }

    @Test
    public void testNegativeSquareRoot() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> cs.execute("sqrt", -25, 0));
    }

    @Test
    public void testUnknownOp() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> cs.execute("unknown", 5, 3));
    }
}
