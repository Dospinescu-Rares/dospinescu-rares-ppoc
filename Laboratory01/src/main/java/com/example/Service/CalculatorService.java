package com.example.Service;


import com.example.Operations.*;

import java.util.Map;
import java.util.HashMap;

public class CalculatorService {
    private final Map<String, Operation> operations = new HashMap<>();

    public CalculatorService(){
        operations.put("+", new Addition());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        operations.put("min", new Minimum());
        operations.put("max", new Maximum());
        operations.put("sqrt", new SquareRoot());
    }

    public int execute(String operator, int a, int b) {
        Operation operation = operations.getOrDefault(operator, (x, y) -> {
            throw new IllegalArgumentException("Operation does not exist!");
        });
        return operation.execute(a, b);
    }
}
