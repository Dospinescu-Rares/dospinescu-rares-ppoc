package com.example;

import com.example.UI.CalculatorUI;

import java.util.InputMismatchException;

public class Main {
    public static void main(String[] args) {
        CalculatorUI ui = new CalculatorUI();
        try {
            ui.run();
        } catch (InputMismatchException e) {
            System.out.print("Error: You need to provide valid integers!\n");
        }
    }
}
