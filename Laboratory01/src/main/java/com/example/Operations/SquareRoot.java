package com.example.Operations;

public class SquareRoot implements Operation {
    @Override
    public int execute(int a, int b) {
        if (a >= 0) {
            return (int) Math.sqrt(a);
        } else {
            throw new IllegalArgumentException("Cannot calculate square root of a negative number");
        }
    }
}
