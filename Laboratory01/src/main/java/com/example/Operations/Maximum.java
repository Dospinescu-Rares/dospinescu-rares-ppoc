package com.example.Operations;

public class Maximum implements Operation {
    @Override
    public int execute(int a, int b) {
        return Math.max(a, b);
    }
}
