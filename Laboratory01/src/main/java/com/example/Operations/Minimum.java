package com.example.Operations;

public class Minimum implements Operation {
    @Override
    public int execute(int a, int b) {
        return Math.min(a, b);
    }
}
