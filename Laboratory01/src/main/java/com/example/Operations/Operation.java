package com.example.Operations;

public interface Operation {
    int execute(int a, int b);
}
