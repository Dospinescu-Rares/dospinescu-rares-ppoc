package com.example.Operations;

public class Division implements Operation {
    @Override
    public int execute(int a, int b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new IllegalArgumentException("Cannot divide by zero");
        }
    }
}
