package com.example.UI;

import com.example.Service.CalculatorService;
import java.util.Scanner;

public class CalculatorUI {
    private final CalculatorService cs;

    public CalculatorUI() {
        cs = new CalculatorService();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String op;
        int result = 0, a = 0, b = 0, reset = 0;

        System.out.println("Calculator CLI");
        System.out.println("Type 'exit' to quit");
        System.out.println("Type 'reset' to reset the calculator");

        while (true) {
            if (reset == 0) {
                System.out.print("Enter the first number: ");
                a = scanner.nextInt();
                System.out.print("Enter the operator: ");
                op = scanner.next();

                reset = 1;
                if (op.equals("exit")) {
                    return;
                }
                else if (op.equals("reset")) {
                    System.out.print("The calculator has been reset to 0\n\n");
                    reset = 0;
                }
                else if (!op.equals("sqrt")) {
                    System.out.print("Enter the second number: ");
                }
                if (op.equals("sqrt")) {
                    b = -1;
                } else {
                    b = scanner.nextInt();
                }

                try {
                    result = cs.execute(op, a, b);
                    System.out.println("Result: " + result);
                } catch (IllegalArgumentException e) {
                    System.out.println("Error: " + e.getMessage());
                }
            }
            else {
                System.out.print("Enter the operator: ");
                op = scanner.next();

                if (op.equals("exit")) {
                    return;
                }
                else if (op.equals("reset")) {
                    System.out.print("The calculator has been reset to 0\n\n");
                    reset = 0;
                    continue;
                }
                else if (!op.equals("sqrt")) {
                    System.out.print("Enter the second number: ");
                }
                if (op.equals("sqrt")) {
                    b = -1;
                } else {
                    b = scanner.nextInt();
                }

                try {
                    result = cs.execute(op, result, b);
                    System.out.println("Result: " + result);
                } catch (IllegalArgumentException e) {
                    System.out.println("Error: " + e.getMessage());
                }
            }
        }
    }
}
