package com.example;

import java.io.*;
import java.net.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class Server {
    private static final int PORT = 6500;
    private static final Set<PrintWriter> writers = new HashSet<>();
    private static final ExecutorService executor = Executors.newFixedThreadPool(16);

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is running...");

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected: " + socket);

                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                writers.add(writer);

                executor.execute(new ClientHandler(socket, writer));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void broadcast(String message) {
        for (PrintWriter writer : writers) {
            writer.println(message);
        }
    }

    static class ClientHandler implements Runnable {
        private final Socket clientSocket;
        private final PrintWriter output;

        public ClientHandler(Socket socket, PrintWriter output) {
            this.clientSocket = socket;
            this.output = output;
        }

        public void run() {
            try {
                BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                String username = input.readLine();
                broadcast("User '" + username + "' has joined.");

                String clientMessage;
                while ((clientMessage = input.readLine()) != null) {
                    if (clientMessage.equalsIgnoreCase("quit")) {
                        break;
                    }
                    broadcast(username + ": " + clientMessage);
                }

                broadcast("User '" + username + "' has left.");

                output.close();
                input.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
