package com.example;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.*;

public class Client {
    private static final String SERVER_ADDRESS = "127.0.0.1";
    private static final int SERVER_PORT = 6500;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            Scanner scanner = new Scanner(System.in);

            System.out.println("Enter your username:");
            String username = scanner.nextLine();
            output.println(username);

            ExecutorService executor = Executors.newFixedThreadPool(16);
            executor.execute(() -> {
                try {
                    String message;
                    while ((message = input.readLine()) != null) {
                        System.out.println(message);
                    }
                } catch (SocketException e) {
                    // Socket closed
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            String userInput;
            while (true) {
                userInput = scanner.nextLine();
                output.println(userInput);
                if (userInput.equalsIgnoreCase("quit")) {
                    break;
                }
            }

            executor.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
